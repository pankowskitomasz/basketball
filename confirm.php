<!DOCTYPE html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0,shrink-to-fit=no"/>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="css/styles.css"/>
        <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
        <link rel="icon" href="img/favicon.png"/>
        <title> | Message sent</title>
    </head>
    <body class="minh-100vh">
        <header class="position-absolute w-100 px-3">
            <nav class="navbar navbar-dark navbar-expand-md bg-transparent">
                <a href="#" class="navbar-brand">
                    <img src="img/navbar_logo.png" class="img-fluid" alt="logo"/>
                </a>
                <button class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#mainnav">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="mainnav">
                    <ul class="navbar-nav text-end ms-auto text-uppercase">
                        <li class="nav-item pe-3">
                            <a href="index.html" class="nav-link text-white text-shadow">Home</a>
                        </li>
                        <li class="nav-item pe-3">
                            <a href="about.html" class="nav-link text-white text-shadow">About</a>
                        </li>
                        <li class="nav-item pe-3">
                            <a href="gallery.html" class="nav-link text-white text-shadow">Gallery</a>
                        </li>
                        <li class="nav-item pe-3">
                            <a href="contact.html" class="nav-link text-white text-shadow">Contact</a>
                        </li>
                        <li class="nav-item pe-3">
                            <a href="user.php" class="nav-link text-white text-shadow"><span class="fa fa-user"></span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <main>            
            <section class="login-s1 container-fluid d-flex align-items-center bg-secondary py-5 minh-100vh">
                <div class="row mx-0 w-100 pt-5 mt-5">
                    <div class="col-12 col-sm-8 col-md-6 offset-sm-2 offset-md-3 text-center">
                        <h3 class="text-center font-header text-secondary">Message sent!</h3>
                        <table class="table table-hover text-left font-menu bg-light opacity-8">
                            <thead class="thead-light">
                                <tr>
                                    <th colspan="2" class="font-header">Summary</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>First name</td>
                                    <td>
                                        <?php 
                                        if(isset($_POST['fname']))
                                            echo htmlspecialchars($_POST['fname']);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Last name</td>
                                    <td>
                                        <?php 
                                        if(isset($_POST['lname']))
                                            echo htmlspecialchars($_POST['lname']);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>
                                        <?php 
                                        if(isset($_POST['fphone']))
                                            echo htmlspecialchars($_POST['fphone']);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>
                                        <?php 
                                        if(isset($_POST['fmail']))
                                            echo htmlspecialchars($_POST['fmail']);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Message</td>
                                    <td>
                                        <?php 
                                        if(isset($_POST['fmsg']))
                                            echo htmlspecialchars($_POST['fmsg']);
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="contact.html" 
                            class="btn btn-outline-secondary font-header">OK</a>
                    </div>
                </div>
            </section>
        </main>
        <footer class="container-fluid d-flex text-dark align-items-center bg-dark text-white pt-3 opacity-9 border-top">
            <div class="row mx-0 w-100 small opacity-9">
                <div class="col-12 col-md-6 col-lg-5 text-center text-md-start">
                    <img src="img/navbar_logo.png" class="img-fluid" alt="logo"/>
                    <p class="initialism fw-normal">
                        We establish the official basketball rules as well as the regulations 
                        that govern the relationships between the different members of the 
                        basketball community. We have five Regional Offices in Africa, Americas, 
                        Asia, Europe and Oceania.
                    </p>
                </div>
                <div class="col-12 col-md-6 col-lg-7 text-center text-md-end">                    
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa fa-facebook text-white"></span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa fa-instagram text-white"></span>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                                <span class="fa fa-twitter text-white"></span>
                            </a>
                        </li>
                    </ul>       
                </div>
                <div class="col-12 text-center border-top">
                    <p class="mb-1">
                        Copyright &copy; 2021-2022 Tomasz Pankowski. 
                        <a href="privacy.html" class="fw-bold text-white text-decoration-none">
                            Privacy policy
                        </a>
                    </p>
                </div>
            </div>
        </footer>
        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>