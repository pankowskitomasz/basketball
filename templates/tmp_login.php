<div class="minh-75vh d-flex">
    <div class="row w-100 my-auto mx-auto">
        <div class="col-11 col-sm-8 col-md-6 col-lg-4 col-xxl-3 mx-auto">
            <div class="card bg-dark border-secondary opacity-9">
                <div class="card-header border-secondary">
                    <h6 class="text-white text-uppercase my-1">
                        Login
                    </h6>
                </div>
                <div class="card-body">
                    <form class="text-start px-3"
                        action=""
                        autocomplete="off"
                        method="post">
                        <div class="form-group mb-3">
                            <label class="text-white mb-2 ms-1">Login</label>
                            <input class="form-control rounded-pill" 
                                name="username"
                                maxlength="45"
                                tabindex="1"
                                type="text" 
                                placeholder="User login"/>
                        </div>
                        <div class="form-group mb-3">
                            <label class="text-white mb-2 ms-1">Password</label>
                            <input class="form-control rounded-pill" 
                                name="userpass"
                                maxlength="45"
                                tabindex="2"
                                required
                                type="text" 
                                placeholder="Enter password"
                                maxlength="80"/>
                        </div>
                        <div class="w-100 small border-bottom text-white">
                            <p>
                                Not registered? Register
                                <a href="register.html" class="fw-bold text-decoration-none text-white">Now</a>
                            </p>
                        </div>
                        <div class="w-100 text-end py-3">
                            <input type="reset" 
                                class="btn btn-light rounded-pill me-1" 
                                value="Clear"
                                tabindex="6"/>
                            <input type="submit" 
                                class="btn btn-light rounded-pill" 
                                value="Login"
                                name="login"
                                tabindex="5"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>        
    </div>
</div>